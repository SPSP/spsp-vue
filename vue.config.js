const GitRevisionPlugin = require('git-revision-webpack-plugin');
process.env.VUE_APP_VERSION = require('./package.json').version
process.env.VUE_APP_RELEASE = require('./package.json').release

module.exports = {
    outputDir: process.env.OUTPUT_DIR,
    devServer: {
        logLevel: 'debug',
        proxy: {
            '/api': {
                target: {
                    host: 'localhost',
                    protocol: 'http',
                    port: 8080
                },
                logLevel: 'debug',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    },
    runtimeCompiler: true,
    transpileDependencies: [
        /\bvue-awesome\b/
    ],
    configureWebpack: {
        plugins: [
            new GitRevisionPlugin()
        ]
    }
};
