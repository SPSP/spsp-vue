# SPSP-FRONTEND

The frontend of SPSP uses the Vue.js framework and several other packages (more info inside `package.json`)

## Getting Started

These following instructions will allow you to understand the structure of the project and change the configuration for your specific needs.

### Prerequisites

What things you need to install the software and how to install them

- **[Node](https://nodejs.org/en/)**
- **[NPM](https://www.npmjs.com/)**

### Installing & running the application

First you will need to install all the dependecies (only the first time)

```
npm install
```

Then, start your node server

```
npm run serve
```

Then your app should be accessible at `http://localhost:8080`

## Customisation

### Changing the colors

You can customize the colors by updating the `/src/css/custom.scss` file.

### Changing the ENV variables

You can change those values by updating the `.env` files

### Changing the columns or options for the dropdowns

All the columns and options are listed in `/src/config/`. You can easily change the `json` files.

## API

### Changing the methods

All the API services are listed in `/src/common/api.service.js`.

## Built With

* [Vuejs](https://vuejs.org/) - The Progressive Javascript Framework

## Contributing

Please contact [Dillenn TERUMALAI](mailto:dillenn.terumalai@sib.swiss) if you would like to contribute to the project.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## License

The source code of the Swiss Pathogen Surveillance Plaftorm software is licensed under GPL-3.0-or-later and available via GitLab.
For more information or any inquiries, please reach out to legal@sib.swiss.

## Authors

* **Dillenn TERUMALAI** - *Initial work* - [Mail](mailto:dillenn.terumalai@sib.swiss)
