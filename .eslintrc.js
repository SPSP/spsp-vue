module.exports = {
    root: true,
    env: {
        node: true
    },
    'extends': [
        'plugin:vue/essential',
        'eslint:recommended'
    ],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-unused-vars': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-multiple-empty-lines': [2, {
            "max": 2,
            "maxEOF": 1,
            "maxBOF": 0
        }],
        "indent": ["error", 4],
        "no-tabs": 0
    },
    parserOptions: {
        parser: 'babel-eslint'
    },
    globals: {
        "_": true
    }
}
