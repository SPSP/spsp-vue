import auth from './auth.json'
import validation from './validation.json'
import team from './team.json'
import sample from './sample.json'
import profile from './profile.json'
import project from './project.json'
import upload from './upload.json'

export default {
    auth,
    validation,
    team,
    sample,
    profile,
    project,
    upload
}