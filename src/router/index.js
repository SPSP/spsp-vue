import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import _ from 'lodash'

Vue.use(Router);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next()
        return
    }
    next('/')
}

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next()
        return
    }

    next('/login')
}

function dynamicPropsFn(route) {
    return {
        id: parseInt(route.params.id)
    }
}


const routes = [
    {
        path: "/",
        name: 'home',
        component: () => import("@/views/Home"),
        beforeEnter: ifAuthenticated,
        meta: {},

    },
    {
        path: '/login',
        name: 'login',
        component: () => import("@/views/Login"),
        beforeEnter: ifNotAuthenticated
    },
    {
        path: '/permission-denied',
        name: 'permissionDenied',
        component: () => import("@/views/PermissionDenied")
    },
    // {
    //     path: '/forgetPassword',
    //     name: 'forgetPassword',
    //     component: () => import("@/views/ForgotPassword")
    // },
    {
        path: '/notifications',
        name: 'notifications',
        component: () => import("@/views/Notifications"),
        beforeEnter: ifAuthenticated,
        meta: {}
    },
    {
        path: '/team',
        name: 'team',
        component: () => import("@/views/Team"),
        beforeEnter: ifAuthenticated,
        meta: {
            functions: ['GROUP_HEAD'],
            role: []
        }
    },
    {
        path: '/profile',
        name: 'profile',
        component: () => import("@/views/Profile"),
        beforeEnter: ifAuthenticated,
        meta: {
        }
    },
    {
        path: '/uploads',
        name: 'uploads',
        component: () => import("@/views/Uploads"),
        beforeEnter: ifAuthenticated,
        meta: {}
    },
    {
        path: '/qc-reviews',
        name: 'qc-reviews',
        component: () => import("@/views/SamplesReview"),
        beforeEnter: ifAuthenticated,
        meta: {
            functions: ['QC_OFFICER'],
            role: []
        }
    },
    {
        path: '/data-browsing',
        name: 'data-browsing',
        component: () => import("@/views/DataBrowsing"),
        beforeEnter: ifAuthenticated,
        meta: {}
    },
    {
        path: '/projects/:id',
        beforeEnter: ifAuthenticated,
        props: dynamicPropsFn,
        component: () => import("@/views/Project"),
        redirect: { name: 'project-summary' },
        children: [
            {
                path: 'summary',
                name: 'project-summary',
                component: () => import("@/components/project-management/ProjectSummary"),
                meta: {}
            },
            {
                path: 'resistance',
                name: 'project-resistance',
                component: () => import("@/components/project-management/ProjectResistance"),
                meta: {}
            },
            {
                path: 'review',
                name: 'project-review',
                component: () => import("@/components/project-management/ProjectReview"),
                meta: {
                    functions: ['QC_OFFICER'],
                    role: []
                }
            },
            {
                path: 'edit',
                name: 'project-edit',
                component: () => import("@/components/project-management/ProjectEdit"),
                meta: {
                    functions: [],
                    role: ['EDITOR']
                }
            },
            {
                path: 'admin',
                name: 'project-admin',
                component: () => import("@/components/project-management/ProjectAdmin"),
                meta: {
                    functions: ['GROUP_HEAD'],
                    role: []
                }
            }
        ]
    },
    {
        path: '/projects/input/create',
        name: 'project-input-create',
        component: () => import("@/views/ProjectAdd"),
        beforeEnter: ifAuthenticated,
        meta: {}
    },
    {
        path: '/projects/monitor/create',
        name: 'project-monitor-create',
        component: () => import("@/views/DataWatchAdd"),
        beforeEnter: ifAuthenticated,
        meta: {}
    },
    {
        path: "*",
        name: 'not-found',
        component: () => import("@/views/NotFound"),
        beforeEnter: ifAuthenticated,
        meta: {}
    }
];

const router = new Router({
    routes: routes,
    mode: "history"
});

router.beforeEach((to, from, next) => {

    if (store.getters.currentUser.firstLogin) {
        next({ name: 'profile' })
    }

    if (to.meta.role && to.meta.functions) {
        var checkRole = false;
        var checkFunction = false;

        //roleExist = true;
        if (!to.meta.role.length || to.meta.role.indexOf(store.getters.currentRole) >= 0) {
            checkRole = true;
        }

        if (!to.meta.functions.length || _.intersection(to.meta.functions, store.getters.currentFunctions).length) {
            checkFunction = true;
        }

        if (checkRole && checkFunction) {
            next();
        } else {
            next({ name: 'permissionDenied' });
        }
    } else {
        next();
    }
});

export default router;
