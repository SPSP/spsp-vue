import Vue from "vue";
import { SET_UPLOADS, UPDATE_UPLOAD, RESET_STATE } from "./mutations.type";
import { FETCH_UPLOADS, ASSIGN_UPLOAD } from "./actions.type";
import { UploadsService } from "@/common/api.service";

const initialState = {
    uploads: []
};

export const state = { ...initialState };

export const getters = {
    uploads: state => state.uploads,
    uploadsCount: (state, getters) => {
        return getters.uploads.length;
    }
};

export const actions = {
    async [FETCH_UPLOADS](context) {
        const response = await UploadsService.get();
        context.commit(SET_UPLOADS, response.data);
    },

    async [ASSIGN_UPLOAD](context, upload) {
        const response = await UploadsService.assign(upload);
        context.commit(UPDATE_UPLOAD, response.data);
    },
    [RESET_STATE]() {
        for (let f in state) {
            Vue.set(state, f, state[f]);
        }
    }
};

const mutations = {
    [SET_UPLOADS]: (state, uploads) => (state.uploads = uploads),
    [UPDATE_UPLOAD]: (state, upload) => {
        const index = state.uploads.findIndex(item => item.id === upload.upload_id);
        if (index !== -1) {
            state.uploads.splice(index, 1);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
