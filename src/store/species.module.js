import Vue from "vue";
import { SET_SPECIES, RESET_STATE } from './mutations.type';
import { FETCH_SPECIES } from './actions.type';
import { SpeciesService } from '@/common/api.service';

const initialState = {
    species: []
};

export const state = { ...initialState };

export const getters = {
    species: state => state.species
};

export const actions = {
    /**
     * Fetch all the species from the API.
     * @param {context} obj
     * @return {response} Success or error.
     */
    async [FETCH_SPECIES](context) {
        const response = await SpeciesService.get();
        context.commit(SET_SPECIES, response.data);
    }
};

export const mutations = {
    [SET_SPECIES]: (state, species) => (state.species = species),
    [RESET_STATE]() {
        for (let f in state) {
            Vue.set(state, f, state[f]);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
