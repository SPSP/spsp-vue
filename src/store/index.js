import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth.module'
import notifications from './notification.module'
import projects from './projects.module'
import species from './species.module'
import team from './team.module'
import uploads from './uploads.module'
import samples from './samples.module'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        auth,
        notifications,
        projects,
        species,
        team,
        uploads,
        samples
    }
});
