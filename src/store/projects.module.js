import Vue from "vue";
import { FETCH_PROJECTS, FETCH_DATAWATCHERS, FETCH_SUMMARY, PROJECT_CREATE, PROJECT_EDIT, DATAWATCHER_CREATE, QUERY_PREVIEW, FETCH_METADATA_TYPES, FETCH_SAMPLES } from './actions.type';
import { SET_PROJECTS, SET_DATAWATCHERS, SET_SUMMARY, SET_SAMPLES, SET_QUERY_RESULTS, RESET_QUERY_RESULTS, PROJECT_ADD, DATAWATCHER_ADD, SET_METADATA_TYPES, RESET_STATE } from './mutations.type';
import { ProjectsService, DataWatchersService, QueryService, MetadataService } from '@/common/api.service';

// State
const initialState = {
    projects: [],
    dataWatchers: [],
    metadataTypes: [],
    summary: {
        id: 0,
        title: "",
        description: null,
        organisms: [],
        nb_of_cases_uploaded: 0
    },
    samples: [],
    previewSamples: []
};

export const state = { ...initialState };

// Getters
export const getters = {
    /**
     * Get all the input projects.
     * @return [input_projects] A list of all input projects
     */
    projects: state => state.projects,

    /**
     * Get all the monitored projects.
     * @return [monitor_projects] A list of all monitored projects
     */
    dataWatchers: state => state.dataWatchers,

    /**
     * Get a specific project.
     * @return {project} A specific project
     */
    summary: state => state.summary,

    /**
     * Get all the samples from a specific projects.
     * @return [samples] A list of all samples
     */
    samples: state => state.samples,

    /**
     * Get all the samples from the query preview.
     * @return [samples] A list of all samples
     */
    previewSamples: state => state.previewSamples,

    /**
     * Get all the samples filtered by quality.
     * @param {quality} String  The quality used for the filter
     * @return [samples] A list of the filtered samples
     */
    projectSamplesFilteredByQuality: getters => (quality) => getters.samples.filter(function (data) {
        return data.Quality == quality;
    }),

    /**
     * Get all the samples filtered by stage.
     * @param {stage} String  The stage used for the filter
     * @return [samples] A list of the filtered samples
     */
    projectSamplesFilteredByStage: getters => (stage) => getters.samples.filter(function (data) {
        return data.stage == stage;
    }),

    /**
     * Count all the monitored projects.
     * @return {integer} Number of monitored projects
     */
    dataWatchersCount: getters => {
        return getters.dataWatchers.length
    },

    /**
     * Count all the input projects.
     * @return {integer} Number of input projects
     */
    projectsCount: getters => {
        return getters.projects.length
    },

    /**
     * Get all the metadata types to build the fields required for the query-builder.
     * @return [metadata_types] A list of the metadata types
     */
    metadataTypes: state => state.metadataTypes
};

export const actions = {
    /**
     * Fetch the input projects from the API.
     * @param {context} obj
     * @return {response} Success or error.
     */
    async [FETCH_PROJECTS](context) {
        const response = await ProjectsService.get();
        context.commit(SET_PROJECTS, response.data);
    },

    /**
     * Fetch the monitor projects from the API.
     * @param {context} obj
     * @return {response} Success or error.
     */
    async [FETCH_DATAWATCHERS](context) {
        const response = await DataWatchersService.get();
        context.commit(SET_DATAWATCHERS, response.data);
    },

    /**
     * Fetch a specific project from the API.
     * @param {context} obj
     * @param {id} integer The id of the requested project
     * @return {response} Success or error.
     */
    async [FETCH_SUMMARY](context, id) {
        const response = await ProjectsService.find(id);
        context.commit(SET_SUMMARY, response.data);
    },

    /**
     * Create an input project.
     * @param {context} obj
     * @param {project} array The form of the input project
     * @return {response} Success or error.
     */
    async [PROJECT_CREATE](context, project) {
        const response = await ProjectsService.create(project);
        context.commit(PROJECT_ADD, response.data);
    },

    /**
     * Update an input project.
     * @param {context} obj
     * @param {project} array The form of the input project
     * @return {response} Success or error.
     */
    async [PROJECT_EDIT](context, project) {
        const response = await ProjectsService.update(project);
        context.commit(SET_SUMMARY, response.data);
    },

    /**
     * Create an monitored project.
     * @param {context} obj
     * @param {project} obj The form of the monitored project
     * @return {response} Success or error.
     */
    async [DATAWATCHER_CREATE](context, project) {
        const response = await DataWatchersService.create(project);
        context.commit(DATAWATCHER_ADD, response.data);
    },

    /**
     * Create an monitored project.
     * @param {context} obj
     * @param {project} array The form of the monitored project
     * @return {response} Success or error.
     */
    async [QUERY_PREVIEW](context, params) {
        const response = await QueryService.preview(params);
        context.commit(RESET_QUERY_RESULTS);
        context.commit(SET_QUERY_RESULTS, response.data);
    },

    /**
     * Fetch the metadata types from the API.
     * @param {context} obj
     * @param {accessLevel} string The access level chosen by the user
     * @return {response} Success or error.
     */
    async [FETCH_METADATA_TYPES](context, accessLevel) {
        const response = await MetadataService.query(accessLevel);
        context.commit(SET_METADATA_TYPES, response.data);
        context.commit(RESET_QUERY_RESULTS);
    },

    /**
     * Fetch the samples of a specific project from the API.
     * @param {context} obj
     * @param {id} integer The id of the requested project
     * @return {response} Success or error.
     */
    async [FETCH_SAMPLES](context, id) {
        const response = await ProjectsService.samples(id);
        context.commit(SET_SAMPLES, response.data);
    }
};

export const mutations = {
    [SET_PROJECTS]: (state, inputProjects) => (state.projects = inputProjects),
    [SET_DATAWATCHERS]: (state, monitorProjects) => (state.dataWatchers = monitorProjects),
    [SET_SUMMARY]: (state, projectSummary) => (state.summary = projectSummary),
    [SET_SAMPLES]: (state, projectSamples) => (state.samples = projectSamples),
    [SET_QUERY_RESULTS]: (state, previewSamples) => (state.previewSamples = previewSamples),
    [RESET_QUERY_RESULTS]: (state) => (state.previewSamples = []),
    [PROJECT_ADD]: (state, inputProject) => state.projects.unshift(inputProject),
    [DATAWATCHER_ADD]: (state, monitorProject) => state.dataWatchers.unshift(monitorProject),
    [SET_METADATA_TYPES]: (state, metadataTypes) => (state.metadataTypes = metadataTypes),
    [RESET_STATE]() {
        for (let f in state) {
            Vue.set(state, f, state[f]);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
