import Vue from "vue";
import {
    NotificationsService
} from "@/common/api.service";
import { FETCH_NOTIFICATIONS, NOTIFICATION_EDIT } from './actions.type';
import { SET_NOTIFICATIONS, UPDATE_NOTIFICATION, RESET_STATE } from './mutations.type';

// State
const initialState = {
    notifications: []
};

export const state = { ...initialState };

// Getters
export const getters = {
    /**
     * Get all the notifications.
     * @return [notifications] A list of all notifications
     */
    allNotifications: state => state.notifications,

    /**
     * Get all the unarchived notifications.
     * @return [unarchived_notifications] A list of unarchived notifications
     */
    unarchivedNotifications: (state, getters) => getters.allNotifications.filter(function (item) {
        return item.status == "UNREAD";
    }),

    /**
     * Get all the archived notifications.
     * @return [archived_notifications] A list of archived notifications
     */
    archivedNotifications: (state, getters) => getters.allNotifications.filter(function (item) {
        return item.status == "READ";
    }),

    /**
     * Count all the unarchived notifications.
     * @return {integer} Number of unarchived notifications
     */
    unarchivedNotificationsCount: (state, getters) => {
        return getters.allNotifications.filter(function (item) {
            return item.status == "UNREAD";
        }).length;
    },

    /**
     * Count all the archived notifications.
     * @return {integer} Number of archived notifications
     */
    archivedNotificationsCount: (state, getters) => {
        return getters.allNotifications.filter(function (item) {
            return item.status == "READ";
        }).length;
    }
};

// Actions
export const actions = {
    /**
     * Fetch the notifications from the API.
     * @param {context} Object
     * @return {response} Success or error.
     */
    async [FETCH_NOTIFICATIONS](context) {
        const response = await NotificationsService.get();
        context.commit(SET_NOTIFICATIONS, response.data)
    },

    /**
     * Update a notification.
     * @param {context} Object
     * @param {notification} Array An Array of the notification.
     * @return {response} Success or error.
     */
    async [NOTIFICATION_EDIT](context, notification) {
        const response = await NotificationsService.update(notification.id, notification.status == "READ" ? "UNREAD" : "READ");
        context.commit(UPDATE_NOTIFICATION, response.data);
    }
};

export const mutations = {
    /**
     * Set the list of notifications.
     * @param {state} Object
     * @param {notification} Array An Array of the notification.
     * @return void
     */
    [SET_NOTIFICATIONS]: (state, notifications) => (state.notifications = notifications),

    /**
     * Remove notification and add new notification.
     * @param {commit} Object
     * @param {user} Array An Array of the notification.
     * @return {response} Success or error.
     */
    [UPDATE_NOTIFICATION]: (state, notification) => {
        const index = state.notifications.findIndex(item => item.id === notification.id);
        if (index !== -1) {
            state.notifications.splice(index, 1, notification);
        }
    },

    /**
     * Reset the full state of the notification module
     */
    [RESET_STATE]() {
        for (let f in state) {
            Vue.set(state, f, state[f]);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
