import ApiService from "@/common/api.service";
import JwtService from "@/common/jwt.service";
import { ProfileService } from "@/common/api.service";
import {
    LOGIN,
    LOGOUT,
    CHECK_AUTH,
    PROFILE_EDIT
} from "./actions.type";
import { SET_AUTH, PURGE_AUTH, SET_ERROR, UPDATE_PROFILE } from "./mutations.type";

const state = {
    errors: null,
    user: {},
    isAuthenticated: !!JwtService.getToken()
};

const getters = {
    currentUser(state) {
        return state.user;
    },
    isAuthenticated(state) {
        return state.isAuthenticated;
    },
    currentRole(state) {
        return state.user.role;
    },
    currentFunctions(state) {
        return state.user.functions;
    }
};

const actions = {
    [LOGIN](context, credentials) {
        return new Promise(resolve => {
            ApiService.post("authentication/signin", { username: credentials.username, password: credentials.password })
                .then(({ data }) => {
                    context.commit(SET_AUTH, data);
                    resolve(data);
                })
                .catch(({ response }) => {
                    context.commit(SET_ERROR, response.data);
                });
        });
    },
    [LOGOUT](context) {
        context.commit(PURGE_AUTH);
    },
    [CHECK_AUTH](context) {
        if (JwtService.getToken()) {
            ApiService.setHeader();
            return ApiService.get("authentication/user")
                .then(({ data }) => {
                    context.commit(SET_AUTH, data);
                })
                .catch((error) => {
                    context.commit(SET_ERROR, error.data);
                    throw error;
                });
        } else {
            context.commit(PURGE_AUTH);
        }
    },
    async [PROFILE_EDIT](context, user) {
        const response = await ProfileService.update(user);
        context.commit(UPDATE_PROFILE, response.data);
    }
};

const mutations = {
    [SET_ERROR](state, errors) {
        state.errors = errors;
    },
    [SET_AUTH](state, user) {
        state.isAuthenticated = true;
        state.user = user;
        state.errors = {};
        JwtService.saveToken(state.user.jwt);
        ApiService.setHeader();
    },
    [PURGE_AUTH](state) {
        state.isAuthenticated = false;
        state.user = {};
        state.errors = {};
        JwtService.destroyToken();
    },
    [UPDATE_PROFILE](state, user) {
        state.user.login = user.login;
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
