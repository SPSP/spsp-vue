import Vue from "vue";
import { SET_MEMBERS, SET_RIGHTS, UPDATE_MEMBER, VALIDATE_MEMBER, RESET_STATE, REJECT_MEMBER } from "./mutations.type";
import { MEMBER_EDIT, MEMBER_VALIDATE, FETCH_MEMBERS, FETCH_RIGHTS, MEMBER_REJECT } from "./actions.type";
import { TeamService } from "@/common/api.service";

// State
const initialState = {
    members: [],
    rights: []
};

export const state = { ...initialState };

// Getters
export const getters = {
    /**
     * Get all the members of the lab.
     * @return [members] A list of all members
     */
    members: state => state.members,

    /**
     * Get all the rights of the current user.
     * @return [rights] A list of all rights
     */
    rights: state => state.rights,

    /**
     * Count all the unregistered members.
     * @return {integer} Number of unregistered members
     */
    unregisteredMembersCount: (getters) => {
        return getters.members.filter(function (member) {
            return member.role == "UNREGISTERED";
        }).length;
    },
};

export const actions = {
    /**
     * Fetch the all the members of the lab.
     * @param {context} Object
     * @return {response} Success or error.
     */
    async [FETCH_MEMBERS](context) {
        const response = await TeamService.get();
        context.commit(SET_MEMBERS, response.data);
    },

    /**
     * Fetch the rights of the current user.
     * @param {context} Object
     * @return {response} Success or error.
     */
    async [FETCH_RIGHTS](context) {
        const response = await TeamService.rights();
        context.commit(SET_RIGHTS, response.data);
    },

    /**
     * Update the status of a member.
     * @param {context} Object
     * @param {member} Object
     * @return {response} Success or error.
     */
    async [MEMBER_EDIT](context, user) {
        const response = await TeamService.update(user);
        context.commit(UPDATE_MEMBER, response.data);
    },

    /**
     * Update the status of a member.
     * @param {context} Object
     * @param {member} Object
     * @return {response} Success or error.
     */
    async [MEMBER_VALIDATE](context, user) {
        const response = await TeamService.validate(user);
        context.commit(VALIDATE_MEMBER, response.data);
    },

    /**
     * Update the status of a member.
     * @param {context} Object
     * @param {member} Object
     * @return {response} Success or error.
     */
    async [MEMBER_REJECT](context, user) {
        const response = await TeamService.reject(user);
        context.commit(REJECT_MEMBER, response.data);
    }
};

export const mutations = {
    [SET_MEMBERS]: (state, members) => (state.members = members),
    [SET_RIGHTS]: (state, rights) => (state.rights = rights),
    [UPDATE_MEMBER]: (state, member) => {
        const index = state.members.findIndex(item => item.user_id === member.user_id);
        if (index !== -1) {
            state.members.splice(index, 1, member);
        }
    },
    [VALIDATE_MEMBER]: (state, member) => {
        const index = state.members.findIndex(item => item.user_id === member.user_id);
        if (index !== -1) {
            state.members.splice(index, 1, member);
        }
    },
    [REJECT_MEMBER]: (state, member) => (state.members = state.members.filter(item => item.user_id !== member.user_id)),
    [RESET_STATE]() {
        for (let f in state) {
            Vue.set(state, f, state[f]);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
