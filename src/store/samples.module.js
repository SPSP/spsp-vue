import Vue from "vue";
import { FETCH_REVIEW_SAMPLES, SAMPLE_VALIDATE } from './actions.type';
import { SET_REVIEW_SAMPLES, SET_PROJECTS_FROM_REVIEW_SAMPLES, REVIEW_SAMPLE_REMOVE, RESET_STATE } from './mutations.type';
import { SamplesService } from '@/common/api.service';

const initialState = {
    reviewSamples: [],
    projectsList: []
};

export const state = { ...initialState };

export const getters = {
    /**
   * Get all the samples to be QC reviewed.
   * @return [samples] A list of all samples
   */
    reviewSamples: state => state.reviewSamples,

    /**
   * Get all the projects name of the samples.
   * @return [projects] A unique list of all projects name.
   */
    projectsList: state => state.projectsList,

    /**
   * Count all the samples to be QC reviewd.
   * @return {integer} Number of samples
   */
    samplesCount: (state, getters) => {
        return getters.reviewSamples.length;
    }
};

export const actions = {
    /**
   * Fetch the sampels to be QC reviewed from the API.
   * @param {context} obj
   * @return {response} Success or error.
   */
    async [FETCH_REVIEW_SAMPLES](context) {
        const response = await SamplesService.get();
        context.commit(SET_REVIEW_SAMPLES, response.data);
        context.commit(SET_PROJECTS_FROM_REVIEW_SAMPLES, response.data);
    },

    /**
   * Validate or reject a sample.
   * @param {context} obj
   * @param {sample} array an array of the sample
   * @return {response} Success or error.
   */
    async [SAMPLE_VALIDATE](context, sample) {
        const response = await SamplesService.validate(sample);
        context.commit(REVIEW_SAMPLE_REMOVE, response.data);
    }
};

export const mutations = {
    /**
   * Set the list of samples.
   * @param {state} obj
   * @param {samples} array An array of the samples.
   * @return void
   */
    [SET_REVIEW_SAMPLES]: (state, samples) => (state.reviewSamples = samples),

    /**
   * Remove the sample that has been validated/rejected.
   * @param {state} obj
   * @param {sample} array An array of the sample.
   * @return void
   */
    [REVIEW_SAMPLE_REMOVE]: (state, sample) => {
        const index = state.reviewSamples.findIndex(item => item.id === sample.id);
        if (index !== -1) {
            state.reviewSamples.splice(index, 1);
        }
    },
    /**
   * Set the list of project names.
   * @param {state} obj
   * @param {samples} array An array of the samples.
   * @return void
   */
    [SET_PROJECTS_FROM_REVIEW_SAMPLES]: (state, samples) => {
        var projects = [];
        samples.forEach(sample => {
            projects.push(sample.project_name)
        });
        var unique_projects = projects.filter((value, index, self) => self.indexOf(value) === index);
        state.projectsList = unique_projects;
    },
    /**
     * Reset the full state of the notification module
     */
    [RESET_STATE]() {
        for (let f in state) {
            Vue.set(state, f, state[f]);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
