import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "@/common/jwt.service";
import { API_URL } from "@/common/config";

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = API_URL;
    },

    setHeader() {
        Vue.axios.defaults.headers.common[
            "Authorization"
        ] = `Bearer ${JwtService.getToken()}`;
    },

    query(resource, params) {
        return Vue.axios.get(resource, params).catch(error => {
            throw new Error(`[RWV] ApiService ${error}`);
        });
    },

    get(resource, id = null) {
        return Vue.axios.get(`${resource}/${id != null ? id : ''}`).catch(error => {
            throw new Error(`[RWV] ApiService ${error}`);
        });
    },

    post(resource, params) {
        return Vue.axios.post(`${resource}`, params);
    },

    update(resource, id, params, action = '') {
        return Vue.axios.patch(`${resource}/${id}${action != "" ? '/' + action : ''}`, params);
    },

    put(resource, params) {
        return Vue.axios.put(`${resource}`, params);
    },

    delete(resource) {
        return Vue.axios.delete(resource).catch(error => {
            throw new Error(`[RWV] ApiService ${error}`);
        });
    }
};

export default ApiService;

export const NotificationsService = {
    get() {
        return ApiService.get("users/notifications");
    },
    update(id, params) {
        if (typeof id !== "number") {
            throw new Error(
                "[RWV] NotificationsService.update() notification id required to update status"
            );
        }
        return ApiService.update("notifications", id, { status: params }, "update_status");
    }
};

export const ProjectsService = {
    get() {
        return ApiService.get("users/input_projects");
    },
    find(id) {
        if (typeof id !== "number") {
            throw new Error(
                "[RWV] ProjectsService.find() project id required to retrieve data"
            );
        }
        return ApiService.get('projects', id);
    },
    create(project) {
        return ApiService.post("projects/input_project/create", {
            title: project.title,
            description: project.description
        });
    },
    update(project) {
        if (typeof project.id !== "number") {
            throw new Error(
                "[RWV] ProjectsService.update() project id required to update profile"
            );
        }
        return ApiService.update('projects', project.id, {
            "title": project.title,
            "description": project.description,
        }, 'update_project');
    },
    samples(id) {
        if (typeof id !== "number") {
            throw new Error(
                "[RWV] ProjectsService.find() project id required to retrieve data"
            );
        }
        return ApiService.get(`/projects/${id}/samples`);
    }
};

export const DataWatchersService = {
    get() {
        return ApiService.get("users/monitor_projects");
    },
    create(dataWatcher) {
        return ApiService.post("projects/monitor_project/create", {
            title: dataWatcher.title,
            description: dataWatcher.description,
            access_level: dataWatcher.target,
            query: dataWatcher.query
        });
    }
};

export const QueryService = {
    preview(params) {
        return ApiService.post("projects/monitor_project/preview", {
            count: false,
            access_level: params.target,
            query: params.query
        });
    }
};

export const MetadataService = {
    query(params) {
        if (typeof params.accessLevel !== "string") {
            throw new Error(
                "[RWV] MetadataService.query() access level required to retrieve metadata"
            );
        }
        return ApiService.query('/cv_metadata_types/summary', { params: { accessLevel: params.accessLevel, is_queryable: true } });
    }
};

export const SamplesService = {
    get() {
        return ApiService.get("/users/samples_to_review");
    },
    validate(sample) {
        if (typeof sample.id !== "number") {
            throw new Error(
                "[RWV] SamplesService.validate() sample id required to validate sample"
            );
        }
        return ApiService.update('samples', sample.id, sample, 'validate');
    }
};

export const SpeciesService = {
    get() {
        return ApiService.get("/samples/mlst_by_species");
    }
};

export const TeamService = {
    get() {
        return ApiService.get("/institutions/users");
    },
    rights() {
        return ApiService.get("/users/validation_rights");
    },
    validate(user) {
        if (typeof user.id !== "number") {
            throw new Error(
                "[RWV] TeamService.validate() user id required to validate a user"
            );
        }
        return ApiService.update('users', user.id, {
            "role": "VIEWER",
            "functions": [],
            "status": "VALID",
        }, 'validate');
    },
    reject(user) {
        if (typeof user.id !== "number") {
            throw new Error(
                "[RWV] TeamService.reject() user id required to reject a user"
            );
        }
        return ApiService.update('users', user.id, {
            "role": null,
            "functions": [],
            "status": "INVALID",
        }, 'validate');
    },
    update(user) {
        if (typeof user.id !== "number") {
            throw new Error(
                "[RWV] TeamService.update() user id required to update functions/rights"
            );
        }
        return ApiService.update('users', user.id, {
            "role": user.role,
            "functions": user.functions,
            "status": "VALID",
        }, 'update_rights');
    }
};

export const UploadsService = {
    get() {
        return ApiService.get("/users/batch_upload_list");
    },
    assign(upload) {
        if (typeof upload.upload_id !== "number") {
            throw new Error(
                "[RWV] UploadsService.assign() upload upload_file_id required to assign upload"
            );
        }
        return ApiService.update('samples/update_upload_project', upload.upload_id, {
            "project_id": parseInt(upload.project)
        });
    }
}

export const ProfileService = {
    update(user) {
        if (typeof user.id !== "number") {
            throw new Error(
                "[RWV] ProfileService.update() user id required to update profile"
            );
        }
        return ApiService.update('users', user.id, {
            "login": user.login,
            "old_password": user.old_password,
            "new_password": user.new_password,
        }, 'update_password');
    }
}
