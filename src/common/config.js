export const API_URL = process.env.VUE_APP_API;          // Check .env file
export const MODE = process.env.VUE_APP_MODE;            // Check .env file
export const INSTANCE = process.env.VUE_APP_INSTANCE;    // Check .env file
export const APP_VERSION = process.env.VUE_APP_VERSION;  // Check package.json
export const APP_RELEASE = process.env.VUE_APP_RELEASE;  // Check package.json
export const SITE_TITLE = "Swiss Pathogen Surveillance Platform";
export const SITE_SHORTNAME = "SPSP";
export const SITE_DESCRIPTION = "The Swiss Pathogen Surveillance Platform connects human and veterinary microbiology laboratories across Switzerland to enable shared surveillance and outbreak analyses based on whole genome sequencing data and associated epidemiological and clinical data.";

export default { API_URL, APP_VERSION, APP_RELEASE, MODE, SITE_TITLE, SITE_SHORTNAME, SITE_DESCRIPTION }
