// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// require('../node_modules/vue-snotify/styles/material.css')

import Vue from 'vue'
import App from './App'
import router from './router'
import store from "./store";

import { CHECK_AUTH, LOGOUT } from "./store/actions.type";
import ApiService from "./common/api.service";
import DateFilter from "./common/date.filter";
import ErrorFilter from "./common/error.filter";
import JwtService from "./common/jwt.service";

import BootstrapVue from 'bootstrap-vue'
import './css/custom.scss'

import Snotify, {
    SnotifyPosition
} from 'vue-snotify'
import 'vue-snotify/styles/material.css'

import 'vue-awesome/icons/envelope';
import 'vue-awesome/icons/chevron-circle-left';
import 'vue-awesome/icons/chevron-right';
import 'vue-awesome/icons/chevron-down';
import 'vue-awesome/icons/user';
import 'vue-awesome/icons/users';
import 'vue-awesome/icons/lock';
import 'vue-awesome/icons/circle';
import 'vue-awesome/icons/check-circle';
import 'vue-awesome/icons/binoculars';
import 'vue-awesome/icons/plus';
import 'vue-awesome/icons/eye';
import 'vue-awesome/icons/edit';
import 'vue-awesome/icons/trash';
import 'vue-awesome/icons/question-circle';
import 'vue-awesome/icons/arrow-left';
import 'vue-awesome/icons/arrow-right';
import 'vue-awesome/icons/chart-bar';
import 'vue-awesome/icons/tasks';
import 'vue-awesome/icons/upload';
import 'vue-awesome/icons/vials';
import 'vue-awesome/icons/vial';
import 'vue-awesome/icons/sign-out-alt';
import 'vue-awesome/icons/bell';
import 'vue-awesome/icons/file-alt';
import 'vue-awesome/icons/times';
import 'vue-awesome/icons/check';
import 'vue-awesome/icons/folder-open';
import 'vue-awesome/icons/plus';
import 'vue-awesome/icons/list';
import 'vue-awesome/icons/database';
import 'vue-awesome/icons/arrow-circle-right';
import 'vue-awesome/icons/heading';
import 'vue-awesome/icons/comment-dots';
import 'vue-awesome/icons/archive';
import 'vue-awesome/icons/inbox';
import 'vue-awesome/icons/globe';
import 'vue-awesome/icons/folder';
import 'vue-awesome/icons/clipboard-check';
import Icon from 'vue-awesome/components/Icon'

import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';

import SummaryCard from '@/components/globals/SummaryCard.vue'
import QueryCard from '@/components/globals/QueryCard.vue'
import ErrorCard from '@/components/globals/ErrorCard.vue'

Vue.config.productionTip = false;
Vue.filter("date", DateFilter);
Vue.filter("error", ErrorFilter);

ApiService.init();

// Ensure we checked auth before each page load. If token is invalid we logout
router.beforeEach((to, from, next) => {
    if (JwtService.getToken()) Promise.all([store.dispatch(CHECK_AUTH)]).then(next).catch(() => store.dispatch(LOGOUT).then(() => {
        Vue.prototype.$snotify.error(
            "Your session has expired, you have been disconnected"
        );
        router.push({ name: "login" })
    }))
    next()
});

// 

Vue.use(Snotify, {
    toast: {
        position: SnotifyPosition.rightTop,
        showProgressBar: false
    }
});
Vue.use(BootstrapVue)
Vue.component('v-select', vSelect)
Vue.component('v-icon', Icon)
Vue.component('summary-card', SummaryCard)
Vue.component('query-card', QueryCard)
Vue.component('error-card', ErrorCard)

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: {
        App
    },
    template: '<App/>'
})
